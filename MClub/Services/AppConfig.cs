﻿namespace MClub.Services
{
    public class AppConfig
    {
        public int SubstrCol { get; set; }
        public int PageSize { get; set; }
    }
}
