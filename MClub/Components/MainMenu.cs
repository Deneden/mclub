﻿using System.Linq;
using MClub.Data;
using Microsoft.AspNetCore.Mvc;

namespace MClub.Components
{
    public class MainMenu : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        public MainMenu(ApplicationDbContext context) => _context = context;
        
        public IViewComponentResult Invoke()
        {
           
            return View(_context.Sections.ToList());
        }
    }
}
