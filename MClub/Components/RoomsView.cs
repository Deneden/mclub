﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MClub.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MClub.Components
{
    public class RoomsView : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        public RoomsView(ApplicationDbContext context) => _context = context;

        public IViewComponentResult Invoke()
        {
            ViewBag.man = " человека";
            ViewBag.mans = " человек";
            var list = _context.Rooms.Include(x => x.Images).ToList();
            list.Reverse(0, list.Count);
            return View(list);
        }
    }
}
