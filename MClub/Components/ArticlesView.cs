﻿using System.Linq;
using MClub.Data;
using MClub.Models.ViewModels;
using MClub.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace MClub.Components
{
    public class ArticlesView : ViewComponent
    {
        private readonly ApplicationDbContext _context;
        private readonly int _pageSize;

        public ArticlesView(ApplicationDbContext context, IOptions<AppConfig> configuration)
        {
            _context = context;
            _pageSize = configuration.Value.PageSize;
        }

        public IViewComponentResult Invoke(int page = 1) => View(new ListViewModel
        {
            Articles = _context.Articles.Include(x => x.Images)
                .OrderByDescending(a => a.CreateTime)
                .Skip((page - 1) * _pageSize)
                .Take(_pageSize),
            PagingInfo = new PagingInfo
            {
                CurrentPage = page,
                ItemsPerPage = _pageSize,
                TotalItems = _context.Articles.Count()
            }

        });

        //View(_context.Articles.Include(x => x.Images).OrderByDescending(a => a.CreateTime).Skip((page - 1) * _pageSize).Take(_pageSize).ToList());
    }
}
