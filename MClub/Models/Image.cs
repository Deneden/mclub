﻿using System.ComponentModel.DataAnnotations;

namespace MClub.Models
{
    public class Image
    {
        [Key]
        public int Id { get; set; }
        public string FileName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string VirtualPath { get; set; }
        public string Path { get; set; }
        public int? ArticleId { get; set; }
        public Article Article { get; set; }
        public int? RoomId { get; set; }
        public Room Room { get; set; }
    }
}
