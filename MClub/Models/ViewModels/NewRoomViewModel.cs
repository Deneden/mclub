﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MClub.Models.ViewModels
{
    public class NewRoomViewModel
    {
        public int Id { get; set; }
        public NewRoomViewModel()
        {
            Images = new List<Image>();
        }
        [Required]
        [Display(Name = "Наименование")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Цена")]
        public int Price { get; set; }
        [Required]
        [Display(Name = "Вместимость")]
        public int? MaxPersons { get; set; }
        [Required]
        [DataType(DataType.Html)]
        [Display(Name = "Описание")]
        public string Description { get; set; }
        public ImagesCheck[] Check { get; set; }
        public ICollection<Image> Images { get; set; }
    }
}
