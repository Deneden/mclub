﻿namespace MClub.Models.ViewModels
{
    public class ImageViewModel
    {
        public string FileName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
