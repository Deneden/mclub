﻿using System.ComponentModel.DataAnnotations;

namespace MClub.Models.ViewModels
{
    public class NewSectionViewModel
    {
        [Required]
        [Display(Name = "Название раздела")]
        public string Name { get; set; }
    }
}
