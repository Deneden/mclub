﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MClub.Models.ViewModels
{
    public class NewArticleViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Заголовок")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "Текст")]
        public string Description { get; set; }
        public ICollection<Image> Images { get; set; }
        [Display(Name = "Раздел")]
        public int? Section { get; set; }
        public ImagesCheck[] Check { get; set; }
        public NewArticleViewModel()
        {
            Images = new List<Image>();
        }
    }
}
