﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MClub.Models.ViewModels
{
    public class ReserveViewModel
    {
        [Display(Name = "Введите ваше имя и фамилию")]
        [Required]
        public string Name { get; set; }
        [Display(Name = "Выберите дату вашего приезда")]
        [Required]
        public DateTime From { get; set; }
        [Display(Name = "Количество дней проживания")]
        public int To { get; set; }
        [Display(Name = "Комментарий к бронированию")]
        public string Comment { get; set; }
        public Room Room { get; set; }
        [Display(Name = "Введите количество человек")]
        public int Persons { get; set; }
    }
}
