﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MClub.Models
{
    public class Room
    {
        public Room()
        {
            Images = new List<Image>();
        }

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int? MaxPersons { get; set; }
        public string Description { get; set; }
        public bool IsRecived { get; set; }
        public DateTime? ReservationDate { get; set; }
        public ICollection<Image> Images { get; set; }
    }
}
