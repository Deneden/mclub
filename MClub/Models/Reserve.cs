﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MClub.Models
{
    public class Reserve
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Persons { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string Comment { get; set; }
        public ApplicationUser User { get; set; }
        public string UserId { get; set; }
        public Room Room { get; set; }
        public int RoomId { get; set; }
    }
}
