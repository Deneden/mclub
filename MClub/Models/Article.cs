﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MClub.Models
{
    public class Article
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreateTime { get; set; }
        public bool IsActive { get; set; }
        public ICollection<Image> Images { get; set; }

        public int? SectionId;

        public Section Section;

        public Article()
        {
            Images = new List<Image>();
        }
    }
}
