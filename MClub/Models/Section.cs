﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MClub.Models
{
    public class Section
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Article> Articles { get; set; }
        public Section()
        {
            Articles = new List<Article>();
        }
    }
}
