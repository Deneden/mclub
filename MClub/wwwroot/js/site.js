﻿Dropzone.options.UploadForm = {
    maxFilesize: 20, // MB
    acceptedFiles: "image/*"
};

$("ul.nav li.dropdown").hover(function () {
        $(this).find(".dropdown-menu").stop(true, true).delay(200).fadeIn(500);
    },
    function () {
        $(this).find(".dropdown-menu").stop(true, true).delay(200).fadeOut(500);
    });


$(document).ready(function () {
    $('a.lightbox').click(function (e) {
        $('body').css('overflow-y', 'hidden'); // hide scrollbars!

        $('<div id="overlay"></div>')
            .css('top', '0')
            .css('opacity', '0')
            .animate({ 'opacity': '0.5' }, 'slow')
            .appendTo('body');

        $('<div id="lightbox"></div>')
            .hide()
            .appendTo('body');
        
        $('<img>')
            .attr('src', $(this).attr('href'))
            .load(function () {
                positionLightboxImage();
            })
            .click(function () {
                removeLightbox();
            })
            .appendTo('#lightbox');

        return false;
    });
});

function positionLightboxImage() {
    var top =  ($(window).height() - $('#lightbox').height()) / 2; //(document.documentElement.clientHeight - document.getElementById('lightbox').offsetHeight)/2;
    var left = ($(window).width() - $('#lightbox').width()) / 2;
    $('#lightbox')
        .css({
            'top': top,
            'left': left
        })
        .fadeIn();
}

function removeLightbox() {
    $('#overlay, #lightbox')
        .fadeOut('slow', function () {
            $(this).remove();
            $('body').css('overflow-y', 'auto'); // show scrollbars!
        });
}
