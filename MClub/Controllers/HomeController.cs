﻿using System.Linq;
using MClub.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MClub.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context) => _context = context;
        public IActionResult Index(int page = 1)
        {
            ViewBag.page = page;
            return View();
        }

        public IActionResult Sections(int id = 0)
        {
            if (id==0)
            {
                id = _context.Sections.FirstOrDefault().Id;
            }

            ViewBag.Section = _context.Sections.FirstOrDefault(x => x.Id == id).Name;

            var list = _context.Articles.Where(x => x.SectionId == id).Include(x => x.Images).ToList();
            list.Reverse(0, list.Count);
            return View(list);
        }

        public IActionResult Articles(int id = 0)
        {
            if (id == 0)
            {
                id = _context.Articles.FirstOrDefault().Id;
            }

            return View(_context.Articles.Where(x => x.Id == id).Include(x => x.Images).FirstOrDefault());
        }


    

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
