using System;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MClub.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using MClub.Data;
using MClub.Models.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace MClub.Controllers
{
    [Authorize(Roles = "Admins")]
    public class AdministrationController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _appEnvironment;

        public AdministrationController(ApplicationDbContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }

        public IActionResult Index() => View(_context.Images.ToList());

        #region SECTIONS

        [HttpGet]
        public IActionResult AddSection()
        {
            ViewBag.Sections = _context.Sections.ToList();

            return View();
        } 

        [HttpPost]
        public IActionResult AddSection(NewSectionViewModel model)
        {
            if (ModelState.IsValid)
            {
                Section section = new Section()
                {
                    Name = model.Name
                };
                _context.Sections.Add(section);
                _context.SaveChanges();
                ViewBag.Sections = _context.Sections.ToList();
                return View();
            }
            ViewBag.Sections = _context.Sections.ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult RenameSelection(int id, NewSectionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var sel = _context.Sections.FirstOrDefault(x => x.Id == id);
                if (sel != null)
                {
                    sel.Name = model.Name;
                    _context.Sections.Update(sel);
                    _context.SaveChanges();
                }
            }
            ViewBag.Sections = _context.Sections.ToList();
            return View("AddSection");
        }

        [HttpPost]
        public IActionResult DeleteSection(int id)
        {
            var sel = _context.Sections.FirstOrDefault(x => x.Id == id);
            if (sel != null)
            {
                _context.Sections.Remove(sel);
                _context.SaveChanges();
            }
            ViewBag.Sections = _context.Sections.ToList();
            return View("AddSection");
        }
        #endregion

        #region ARTICLES

        [HttpGet]
        public IActionResult AddArticle()
        {
            SelectList sections = new SelectList(_context.Sections.ToList(), "Id", "Name", 1);
            ViewBag.Articles = _context.Articles.ToList();
            ViewBag.Sections = sections;
            return View();
        }

        [HttpPost]
        public IActionResult PublicArticle(int id)
        {
            var sel = _context.Articles.FirstOrDefault(x => x.Id == id);
            if (sel != null)
            {
                sel.IsActive = sel.IsActive ? sel.IsActive = false : sel.IsActive = true;
                _context.Articles.Update(sel);
                _context.SaveChanges();             
            }
            SelectList sections = new SelectList(_context.Sections.ToList(), "Id", "Name", 1);
            ViewBag.Articles = _context.Articles.ToList();
            ViewBag.Sections = sections;
            return View("AddArticle");
        }

        [HttpPost]
        public IActionResult DeleteArticle(int id)
        {
            var sel = _context.Articles.FirstOrDefault(x => x.Id == id);
            if (sel != null)
            {
                _context.Articles.Remove(sel);
                _context.SaveChanges();
            }
            SelectList sections = new SelectList(_context.Sections.ToList(), "Id", "Name", 1);
            ViewBag.Articles = _context.Articles.ToList();
            ViewBag.Sections = sections;
            return View("AddArticle");
        }

        [HttpGet]
        public IActionResult EditArticle(int id)
        {
            
            var article = _context.Articles.Include(x=>x.Images).FirstOrDefault(x => x.Id == id);

            if (article != null)
            {
                NewArticleViewModel model = new NewArticleViewModel()
                {
                    Description = article.Description,
                    Images = article.Images,
                    Section = article.SectionId,
                    Title = article.Title,
                    Id = article.Id
                };

                SelectList sections = new SelectList(_context.Sections.ToList(), "Id", "Name");
                if (sections.Any()) sections.First(x => x.Value == model.Section.ToString()).Selected = true;
                ViewBag.Sections = sections;

                return View(model);
            }
            return Content("������ �� �������");
        }

        [HttpPost]
        public async Task<IActionResult> EditArticle(NewArticleViewModel model)
        {
            if (ModelState.IsValid)
            {
                Article article = _context.Articles.Include(x=>x.Images).FirstOrDefault(x => x.Id == model.Id);

                if (article != null)
                {
                    var files = HttpContext.Request.Form.Files;
                    try
                    {
                        foreach (var uploadedFile in files)
                        {
                            var fileName = uploadedFile.FileName;
                            var virtualPath = "/images/" + fileName;

                            var path = Path.Combine(_appEnvironment.WebRootPath, "images", fileName);

                            using (var fileStream = new FileStream(path, FileMode.Create))
                            {
                                await uploadedFile.CopyToAsync(fileStream);
                            }
                            var newfile = new Image
                            {
                                FileName = fileName,
                                Path = path,
                                Title = "",
                                Description = "",
                                VirtualPath = virtualPath
                            };

                            _context.Images.Add(newfile);
                            article.Images.Add(newfile);
                        }

                        if(model.Check!=null) {
                        foreach (var img in model.Check)
                        {
                            if (img.Delete)
                            {
                                var delImg = article.Images.FirstOrDefault(x => x.Id == img.Id);
                                if(delImg!=null) article.Images.Remove(delImg);
                            }
                        }
                        }
                        article.Description = model.Description;
                        article.Title = model.Title;
                        article.SectionId = model.Section;
                        _context.Articles.Update(article);
                        _context.SaveChanges();

                    }
                    catch (Exception ex)
                    {
                        return Content($"{ex.Message}, {ex.StackTrace}");
                    }
                }       
                return RedirectToAction("Articles", "Home", new {id=model.Id});
            }

            SelectList sections = new SelectList(_context.Sections.ToList(), "Id", "Name");
            if(sections.Any()) sections.First(x => x.Value == model.Section.ToString()).Selected = true;
            ViewBag.Sections = sections;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddArticle(NewArticleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var files = HttpContext.Request.Form.Files;
                try
                {
                    foreach (var uploadedFile in files)
                    {
                        var fileName = uploadedFile.FileName;
                        var virtualPath = "/images/" + fileName;

                        var path = Path.Combine(_appEnvironment.WebRootPath, "images", fileName);

                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            await uploadedFile.CopyToAsync(fileStream);
                        }
                        var newfile = new Image
                        {
                            FileName = fileName,
                            Path = path,
                            Title = "",
                            Description = "",
                            VirtualPath = virtualPath
                        };
                        _context.Images.Add(newfile);
                        model.Images.Add(newfile);
                    }
                }
                catch (Exception ex)
                {
                    return Content($"{ex.Message}, {ex.StackTrace}");
                }

                Article article = new Article
                {
                    Title = model.Title,
                    Description = model.Description,
                    Images = model.Images,
                    Section = _context.Sections.FirstOrDefault(x => x.Id == model.Section),
                    SectionId = model.Section,
                    IsActive = true,
                    CreateTime = DateTime.Now
                };


                _context.Articles.Add(article);
                _context.SaveChanges();

                return RedirectToAction("Articles", "Home");
            }

            SelectList sections = new SelectList(_context.Sections.ToList(), "Id", "Name", 1);

            ViewBag.Sections = sections;
            ViewBag.Articles = _context.Articles.ToList();

            return View("AddArticle", model);
        }

        #endregion

        #region ROOMS

        [HttpGet]
        public IActionResult AddRoom()
        {
            ViewBag.Rooms = _context.Rooms.Include(x => x.Images).ToList();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddRoom(NewRoomViewModel model)
        {
            if (ModelState.IsValid)
            {


                var files = HttpContext.Request.Form.Files;
                try
                {
                    foreach (var uploadedFile in files)
                    {
                        var fileName = uploadedFile.FileName;
                        var virtualPath = "/images/" + fileName;
                        
                        var path = Path.Combine(_appEnvironment.WebRootPath, "images", fileName);

                        using (var fileStream = new FileStream(path, FileMode.Create))
                        {
                            await uploadedFile.CopyToAsync(fileStream);
                        }
                        var newfile = new Image
                        {
                            FileName = fileName,
                            Path = path,
                            Title = "",
                            Description = "",
                            VirtualPath = virtualPath
                        };
                        _context.Images.Add(newfile);
                        model.Images.Add(newfile);
                    }
                }
                catch (Exception ex)
                {
                    return Content($"{ex.Message}, {ex.StackTrace}");
                }

                Room room = new Room
                {                  
                    Name = model.Name,
                    Description = model.Description,
                    IsRecived = false,
                    MaxPersons = model.MaxPersons,
                    Price = model.Price,
                    Images = model.Images
                };


                _context.Rooms.Add(room);
                _context.SaveChanges();
                return RedirectToAction("Rooms", "Home");
            }
            ViewBag.Rooms = _context.Rooms.Include(x => x.Images).ToList();
            return View("AddRoom",model);
        }

        [HttpPost]
        public IActionResult DeleteRoom(int id)
        {
            var sel = _context.Rooms.FirstOrDefault(x => x.Id == id);
            if (sel != null)
            {
                _context.Rooms.Remove(sel);
                _context.SaveChanges();
            }

            ViewBag.Rooms = _context.Rooms.Include(x=>x.Images).ToList();
            return View("AddRoom");
        }

        [HttpPost]
        public IActionResult ReserveRoom(int id)
        {
            var sel = _context.Rooms.FirstOrDefault(x => x.Id == id);
            if (sel != null)
            {
                sel.IsRecived = sel.IsRecived ? sel.IsRecived = false : sel.IsRecived = true;
                _context.Rooms.Update(sel);
                _context.SaveChanges();
            }

            ViewBag.Rooms = _context.Rooms.Include(x=>x.Images).ToList();
            return View("AddRoom");
        }

        [HttpGet]
        public IActionResult EditRoom(int id)
        {

            var room = _context.Rooms.Include(x => x.Images).FirstOrDefault(x => x.Id == id);

            if (room != null)
            {
                NewRoomViewModel model = new NewRoomViewModel()
                {
                    Name = room.Name,
                    Description = room.Description,
                    Images = room.Images,
                    Price = room.Price,
                    MaxPersons = room.MaxPersons,
                    Id = room.Id
                };

                return View(model);
            }
            return Content("������ �� �������");
        }

        [HttpPost]
        public async Task<IActionResult> EditRoom(NewRoomViewModel model)
        {
            if (ModelState.IsValid)
            {
                Room room = _context.Rooms.Include(x => x.Images).FirstOrDefault(x => x.Id == model.Id);

                if (room != null)
                {
                    var files = HttpContext.Request.Form.Files;
                    try
                    {
                        foreach (var uploadedFile in files)
                        {
                            var fileName = uploadedFile.FileName;
                            var virtualPath = "/images/" + fileName;

                            var path = Path.Combine(_appEnvironment.WebRootPath, "images", fileName);

                            using (var fileStream = new FileStream(path, FileMode.Create))
                            {
                                await uploadedFile.CopyToAsync(fileStream);
                            }
                            var newfile = new Image
                            {
                                FileName = fileName,
                                Path = path,
                                Title = "",
                                Description = "",
                                VirtualPath = virtualPath
                            };

                            _context.Images.Add(newfile);
                            room.Images.Add(newfile);
                        }

                        if (model.Check != null)
                        {
                            foreach (var img in model.Check)
                            {
                                if (img.Delete)
                                {
                                    var delImg = room.Images.FirstOrDefault(x => x.Id == img.Id);
                                    if (delImg != null) room.Images.Remove(delImg);
                                }
                            }
                        }
                        room.Description = model.Description;
                        room.Name = model.Name;
                        room.MaxPersons = model.MaxPersons;
                        room.Price = model.Price;
                        _context.Rooms.Update(room);
                        _context.SaveChanges();

                    }
                    catch (Exception ex)
                    {
                        return Content($"{ex.Message}, {ex.StackTrace}");
                    }
                }
                return RedirectToAction("Rooms", "Home", new { id = model.Id });
            }

            return View(model);
        }
        #endregion

        #region RECERVE

        [HttpGet]
        public IActionResult Reserve()
        {
            var reserves = _context.Reserves.Include(x=>x.User).Include(x=>x.Room).ToList();
            return View(reserves);
        }

        [HttpPost]
        public IActionResult DeleteReserve(int id)
        {
            var reserve = _context.Reserves.Include(x=>x.Room).FirstOrDefault(x => x.Id == id);
            if (reserve!= null)
            {
                var room = reserve.Room;
                room.IsRecived = false;
                _context.Rooms.Update(room);
                _context.Reserves.Remove(reserve);
                _context.SaveChanges();
            }
            var reserves = _context.Reserves.Include(x => x.User).Include(x => x.Room).ToList();

            return View("Reserve",reserves);
        }

        [HttpGet]
        public IActionResult EditReserve(int id)
        {
            var reserve = _context.Reserves.FirstOrDefault(x => x.Id == id);
            if (reserve != null)
            {
                return View(reserve);
            }
            return Content("������ �� �������");
        }
        #endregion
        public async Task<IActionResult> AddFile()
        {
            var files = HttpContext.Request.Form.Files;
            var fileName = "";

            try
            {
                foreach (var uploadedFile in files)
                {
                    fileName = uploadedFile.FileName;
                    var virtualPath = "/images/" + fileName;

                    var path = Path.Combine(_appEnvironment.WebRootPath, "images", fileName);

                    using (var fileStream = new FileStream(path, FileMode.Create))
                    {
                        await uploadedFile.CopyToAsync(fileStream);
                    }
                    var newfile = new Image
                    {
                        FileName = fileName,
                        Path = path,
                        Title = "",
                        Description = "",
                        VirtualPath = virtualPath
                    };
                    _context.Images.Add(newfile);
                }
                _context.SaveChanges();
            }

            catch (Exception ex)
            {
                return Json(new {Message = ex});
            }
            return Json(new {Message = fileName});
        }
    }
}