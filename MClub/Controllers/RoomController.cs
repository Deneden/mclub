using System;
using System.Linq;
using System.Threading.Tasks;
using MClub.Data;
using MClub.Models;
using MClub.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MClub.Controllers
{
    [Authorize]
    public class RoomController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _manager;
        private Task<ApplicationUser> GetCurrentUserAsync() => _manager.GetUserAsync(HttpContext.User);
        public RoomController(ApplicationDbContext context, UserManager<ApplicationUser> manager)
        {
            _context = context;
            _manager = manager;
        } 
        
        public IActionResult Index() => View(_context.Rooms.Include(p=>p.Images).ToList());

        [HttpGet]
        public IActionResult Reserve(int id)
        {
            Room room = _context.Rooms.FirstOrDefault(x => x.Id == id);

            ReserveViewModel model = new ReserveViewModel()
            {
                From = DateTime.Now.AddDays(1),
                Room = room
            };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Reserve(ReserveViewModel model, int id)
        {
            if (ModelState.IsValid)
            {
                Reserve reserve = new Reserve()
                {
                    Comment = model.Comment,
                    From = model.From,
                    To = model.From.AddDays(model.To),
                    Name = model.Name,
                    Persons = model.Persons,
                    Room = _context.Rooms.FirstOrDefault(x => x.Id == id),
                    User = await GetCurrentUserAsync()
                };

                reserve.Room.IsRecived = true;
                _context.Rooms.Update(reserve.Room);
                _context.Reserves.Add(reserve);
                _context.SaveChanges();
                return View("ThankYou");
            }
            return View(model);
        }

        public IActionResult Rooms(int id = 0)
        {
            if (id == 0)
            {
                id = _context.Rooms.FirstOrDefault().Id;
            }

            return View(_context.Rooms.Where(x => x.Id == id).Include(x => x.Images).FirstOrDefault());
        }
    }
}